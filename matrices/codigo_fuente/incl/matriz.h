#include<stdlib.h>
#include<stdio.h>

int** crear_matriz(int filas, int columnas);

int borrar_matriz(int filas,int ** matriz);

int**  limpiar_matriz(int filas, int columnas, int*** matriz);

int** suma_matriz_escalar(int filas,int columnas, int suma,int*** matriz);

int** multiplicar_matris_escalar(int filas,int columnas, int multiplicador,int*** matriz);

int** transponer_matriz(int filas, int columnas, int*** matriz);

int** suma_matrices(int filas, int columnas, int*** matriz1, int*** matriz2);

int** multiplicador_matrices(int filas1, int columnas1, int*** matriz, int filas2,int columnas2, int*** matriz1);
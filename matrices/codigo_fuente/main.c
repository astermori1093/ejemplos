#include "matriz.h"
int main(){
//declarando matrices 
int **matriz;
int **matriz3;
int **matriz2;
int **matriz_matriz;
//creando matrices
matriz =crear_matriz(5,5);
matriz2 =crear_matriz(5,5);
matriz3=crear_matriz(5,5);
matriz_matriz=crear_matriz(3,5);
//borrado una matriz
borrar_matriz(5,matriz3);
//sumando un escalar a una matriz  
matriz=suma_matriz_escalar(5,5,3,&matriz);
//multiplicando  un escalar a una matriz
matriz2=multiplicar_matris_escalar(5,5,5,&matriz);
//limpiando una matriz
matriz=limpiar_matriz(5,5,&matriz);
//transponer una matriz
*(*(matriz+1)+2)=1;
matriz=transponer_matriz(5,5,&matriz);
//sumando 2 matrices
matriz2=suma_matrices(5,5,&matriz, &matriz2);
///dondo le valores a matriz_matriz
matriz_matriz=suma_matriz_escalar(3,5,3,&matriz_matriz);
///multiplicando matrices 
matriz=multiplicador_matrices(3,5,&matriz_matriz,5,5,&matriz2);
matriz=multiplicador_matrices(5,5,&matriz2,3,5,&matriz_matriz);
return 0;
}
/**
* @file laberinto.h
* @Author David Andres Cubero Morera
* @date 9/6/2019
* @brief laberinto.c.h implementation
*/
#ifndef lABERINTO
#define LABERINTO
#include"raton.h"

/**
 * @brief crear_matriz_dinamica crea una matriz de tipo char  de dimensiones 5x5
 * @return devuelve una matriz dinamica vaciá de dimensiones de  5x5
 */
char** crear_matriz_dinamica();
/**
 * @brief cargar_laberinto  carga una matriz de dimensiones 5x5 de tipo char
 * @return devuelve una matriz de tipo char de dimensiones 5x5
 */
char** cargar_laberinto();
/**
 * @brief imprimir_laberinto  imprime el contenido de la matriz 5x5
 * @param filas numero de filas de la matriz
 * @param columnas numero de columnas de la matriz
 * @param laberinto direccion de memoria de la matriz laberinto de dimensiones 5x5
 * @return devuelve una matriz de tipo char de dimensiones 5x5 
 */
void imprimir_laberinto(int filas,int columnas,char *** laberinto);
#endif
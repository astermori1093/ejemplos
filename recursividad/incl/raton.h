/**
* @file raton.h
* @Author David Andres Cubero Morera
* @date 9/6/2019
* @brief raton.h implementation
*/
#ifndef RATON
#define RATON
#include"laberinto.h"
#include<stdlib.h>
#include<stdio.h>
/**
 * 
 */
typedef struct 
{
    int posicion_x;
    int posicion_y;
} raton;

/**
 * @brief nace_raton crear una variable de dinamica de tipo raton
 * @return devuelve de un puntero de tipo raton 
 */
raton* nace_raton();
/**
 * @brief buscarRaton busca el string "r" dentro de la matriz
 * @param filas numero de filas de la matriz
 * @param columnas numero de columnas de la matriz
 * @param laberinto direccion de memoria de la matriz laberinto de dimensiones 5x5
 * @return devuelve de un puntero de tipo raton con las posiciones iniciales del string "r"
 */
raton* buscarRaton(int filas, int columnas,char ***laberinto);
/**
 * @brief buscarQueso funcion recursiva que permite que el string "r" llegue al string "Q" de una matriz 5x5
 * @param filas numero de filas de la matriz
 * @param columnas numero de columnas de la matriz
 * @param laberinto direccion de memoria de la matriz laberinto de dimensiones 5x5
 */
void buscarQueso(int filas,int columnas,char ***laberinto);

#endif
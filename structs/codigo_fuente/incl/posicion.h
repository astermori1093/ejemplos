#ifndef POSICION
#define POSICION
#include<stdlib.h>
#include<stdio.h>
/**
 *@brief struct posicion se se encarga de crear la estructura de posicion que va a tener una entrada en la lista
 *parametros anterior y siguiente son los que se encargan de colocar la nueva posicion antes o despues de la posicion actual 
*/
struct posicion{
    unsigned long long dato;
    struct posicion*  siguiente; 
    struct posicion* anterior;
};
/**
 *@brief Aqui se declaran las funciones que se programaron en el archivo posicion.c
 *cada funcion se declaro como void dado que no retorna una salida.
*/
void eliminar_posicion(struct posicion* posicion_puntero);
struct posicion* crear_posicion(int j);
struct posicion* anterior(struct posicion *posicion_puntero);
struct posicion* siguiente(struct posicion *posicion_puntero);

#endif
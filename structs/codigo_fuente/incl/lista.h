#ifndef LISTA
#define LISTA
#include<stdlib.h>
#include<stdio.h>
#include"posicion.h"

struct lista
{
    unsigned int items; 
    struct posicion * primero;
};
/**
 *@brief crear_lista crea una struct de tipo lista
 *@return devuelve el puntero a dirección de memoria donde se encuentra la lista creada
*/
struct lista* crear_lista();  
/**
 *@brief imprimir_lista imprime un struct de tipo lista
 *param lista_puntero el puntero que apunta al struct lista
 *@return imprime cada uno de los elementos de lista
*/
void imprimir_lista(struct lista* lista_puntero); //imprime lista
/**
 *@brief agregar_elemento agrega un elememento de tipo posición a la estructura lista, en pocas palabras los enlaza
 *param lista_puntero un puntero que apunta al struct lista
 *@return devuelve un puntero de tipo lista
*/
struct lista * agregar_elemento(struct lista *lista_puntero,int valor); //agrega un elemento a lista
/**
 *@brief vaciar_lista libera la memoria de las struct de tipo posición dentro del struct lista 
 *param lista_puntero el puntero que apunta a la  estructura lista
 *@return no devuelve nada
*/
void vaciar_lista(struct lista *lista_puntero);//elimina las estructuras posición
/**
 *@brief eliminar_lista libera la memoria asignada a la  struct lista
 *param lista_puntero el puntero que apunta a la  estructura lista
 *@return no devuelve nada
*/
void eliminar_lista(struct lista *lista_puntero);///vaciar lista
/**
 *@brief eliminar_elemento_lista elimina una de las structs de posicion enlazados a la lista
 *param lista_puntero el puntero que apunta a la  estructura lista
 *@return no devuelve nada
*/
void eliminar_elemento_lista(struct lista* lista_puntero, int posicion);///elimina un elemento de la lista
/**
 *@brief buscar_dato  busca un dato en la lista
 *param lista_puntero el puntero que apunta a la  estructura lista
 *@return no devuelve nada
*/
void buscar_dato(struct lista* lista_puntero, int x);//busca un dato
/**
 *@brief buscar_k busca la dirección de memoria de una posición en especifico
 *param lista_puntero el puntero que apunta a la  estructura lista
 *param posicion_puntero
 *@return no devuelve nada
*/
int buscar_k(struct lista* lista_puntero, struct posicion* posicion_puntero);
#endif
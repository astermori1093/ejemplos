#include"posicion.h"

/**
*@brief Siguiente: La función siguiente devuelve un puntero a la posición siguiente que apunta la estructura de entrada.
*/
struct posicion* siguiente(struct posicion *posicion_puntero){
struct posicion* temporal=posicion_puntero->siguiente;

printf("direccion de memoria de posicion_puntero %p\n",posicion_puntero);
printf("direccion de memoria de que deseamos que retorne %p\n\n",posicion_puntero->siguiente);
printf("direction de memoria que retorna la función %p\n",temporal);

return temporal;
}

/**
*@brief Anterior: La función anterior es homologa a la función siguiente, dado que crea una posicion
*nueva anterior a la posicion actual en la que esta colocado el puntero. . 
*/

struct posicion* anterior(struct posicion *posicion_puntero){
struct posicion* temporal=posicion_puntero->anterior;
printf("dirección de memoria de posicion_puntero %p\n",posicion_puntero);
printf("dirección de memoria de que deseamos que retorne %p\n\n",posicion_puntero->siguiente);
printf("direction de memoria que retorna la función %p\n",temporal);

return temporal;
}


/**
 *@brief Crear posicion: esta función se encarga de generar una nueva posicion en la lista. Por defecto,
 *esta función agrega la nueva posicion al final de la lista. 
 */
struct posicion* crear_posicion(int j){
    struct posicion* nueva_posicion = (struct posicion*)malloc(sizeof(struct posicion));
    nueva_posicion->dato = j;
    nueva_posicion->siguiente;
    nueva_posicion->anterior;
    if(nueva_posicion==NULL){
        printf("NO SE CREO LA NUEVA POSICON");
    }
    return nueva_posicion;
    printf("se creo posicion");
}

/**
 *@brief Eliminar posicion: esta funcion se encarga de eliminar una posicion especifica de la lista, indicandole
 *a la funcion la posicion que se desea eliminar.
 */

void eliminar_posicion(struct posicion* posicion_puntero){
    free(posicion_puntero);
    printf("se elimino el struct de tipo posicion\n");
}


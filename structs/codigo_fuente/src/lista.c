#include "lista.h"
#include "posicion.h"
/**@brief Crear_lista: esta funcion es la principal encargada de crear el objeto lista
 */
struct lista *crear_lista() 
{
    struct lista *l = (struct lista *)malloc(sizeof(struct lista)); 
    l->items = 0;                                                   
    l->primero = 0x0;
    if (l == NULL)
    {
        printf("NO SE LE ASIGNO EL VALOR A L");
    }

    return l;
}
/**@brief Agregar elemento: esta funcion se ocupa de agregar una nueva entrada a la lista
 * ademas de asignanerle una posicion.
 */
struct lista *agregar_elemento(struct lista *lista_puntero, int valor)
{
    struct posicion *nueva_cajita = (struct posicion *)malloc(sizeof(struct posicion));

    if (lista_puntero->primero == NULL){
        lista_puntero->items++;
        lista_puntero->primero = nueva_cajita;
        lista_puntero->primero->dato = valor;
        lista_puntero->primero->siguiente = NULL;
        lista_puntero->primero->anterior = NULL; 
    }
    else{
        struct posicion *temporal = lista_puntero->primero;
        while (temporal->siguiente != NULL){
            temporal = temporal->siguiente;
        }
        lista_puntero->items++;
        nueva_cajita->dato = valor;
        nueva_cajita->siguiente = NULL;
        nueva_cajita->anterior = temporal;
        temporal->siguiente = nueva_cajita;
    }
    return lista_puntero;
}
/**@brief Imprimir lista: esta funcion imprime la lista que se ha creado en la terminal */
void imprimir_lista(struct lista *lista_puntero)
{
    struct posicion *temporal; 
    temporal = lista_puntero->primero;
    for (int i = 0; i < lista_puntero->items; i++)
    {
        int j = i;
        printf("en el item %i\n", (j + 1));
        printf("el valor es %i\n", temporal->dato);
        temporal = temporal->siguiente;
    }
}
/**@brief Vaciar lista: se encarga de eliminar las entradas que hay en la lista. */
void vaciar_lista(struct lista *lista_puntero)
{
if(lista_puntero->primero==NULL){
    printf("la lista es null");
}
else{
    struct posicion *temporal,*auxiliar;
    temporal=lista_puntero->primero;
    printf("elementos lista %i\n",lista_puntero->items);
    for(int i=0;i<lista_puntero->items; i++ ){
        printf(" en elemento %i\n",i);
        auxiliar=temporal->siguiente;
        free(temporal);
        temporal=auxiliar;
    }
    lista_puntero->items=0;
}
}
/**@brief Eliminar lista: Esta funcion es similar a la funcion anterior, pero esta se encarga de eliminar
 * el objeto lista.
 */
void eliminar_lista(struct lista *lista_puntero){
    vaciar_lista(lista_puntero);
    free(lista_puntero);
    printf("se elimino  la lista\n");
}
/**@brief Eliminar elemento: elimina un elemento especifico de la lista. */
void eliminar_elemento_lista(struct lista* lista_puntero, int posicion){
    struct posicion* temporal;
    struct posicion* auxiliar1=(struct posicion *)malloc(sizeof(struct posicion));
    struct posicion* auxiliar2=(struct posicion *)malloc(sizeof(struct posicion));
    struct posicion* nueva_cajita=(struct posicion *)malloc(sizeof(struct posicion));
    temporal=lista_puntero->primero;
    for(int i=1;i<posicion;i++){
        temporal=temporal->siguiente;
        if(i==posicion-1){
            auxiliar1=temporal->anterior;
            auxiliar2=temporal->siguiente;
            free(temporal);
            temporal=auxiliar1;
            if(auxiliar2==NULL){
            temporal->siguiente=nueva_cajita;
            temporal=nueva_cajita;
            temporal->anterior=auxiliar1;
            free(auxiliar2);
            }
        else{
            temporal->siguiente=auxiliar2;
            temporal=auxiliar2;
            temporal->anterior=auxiliar1;
            }
        lista_puntero->items=lista_puntero->items-1;
        }
       
    }
}
/**@brief Buscar dato: Se encarga de buscar un dato especifico de la lista. */
void buscar_dato(struct lista * lista_puntero, int x){
    struct posicion* temporal = lista_puntero->primero;
    for (int i=0;i<lista_puntero->items;i++){ 
        
        if (temporal->dato == x){
            printf("Se encontró el dato en la lista \n  ");
            printf("Esta en la posicion %i \n\n",i+1);
            i=lista_puntero->items-1;

         }
        else
        {
           temporal = temporal->siguiente; 
           printf("No se encuentra el dato en la lista\n");
        }
    }
}  

/**@brief Buscar k: Imprime la direccion hexadeximal de memoria de un objeto especifico */
int buscar_k(struct lista* lista_puntero, struct posicion* posicion_puntero){
    struct posicion* temporal= lista_puntero->primero;
    for(int i=0;i<lista_puntero->items;i++){
        if(temporal==posicion_puntero){
            printf("el puntero apunta a la dirección de memoria %p\n",&temporal );
            printf("el dato en esa posicion es %i\n",temporal->dato);
            printf("es el item %i la lista\n",i+1);
            i=lista_puntero->items-1;
        }
        else
        {
            printf("no se encuentra en el item %i\n\n",i+1);
            temporal= temporal->siguiente;
        }
        
    }
    return 0;
}
  /* if (isEmpty(l)) {
      puts("La lista esta vacia.\n\n");
   } 
   else { 
      puts("La lista es:\n\n");
      while (l != NULL) { 
         printf("%s %p --> ", p->dato, &p);
         p = p->siguiente;   
      } 
      puts("NULL\n\n");
   } 
}

int isEmpty(struct lista* l) {*/

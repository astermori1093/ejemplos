#include "lista.h"
#include "posicion.h"  
/**
 * @brief Archivo main: Este archivo se encarga de ejecutar las instrucciones programadas en los archivos
 * de posicion y lista, llamando a los archivos .h . Para esto, se realizan funciones de prueba que verifiquen
 * que, efectivamente, el codigo es funcional. Se prueban todas las funciones realizadas. */ 

int main(){
printf("Probando posicion\n");
struct posicion* posicion_posicon= crear_posicion(3);
printf("el valor de posicion->dato es %i\n",posicion_posicon->dato);
printf("el valor de posicion->anterior es %i\n",posicion_posicon->anterior);
printf("el valor de posicion->siguiente es %i\n",posicion_posicon->siguiente);
printf("\n\n\n");
printf("probando posicion siguiente\n\n");
struct posicion* posicion_1=siguiente(posicion_posicon);
printf("probando posicion anterior \n\n");
struct posicion* posicion_2=anterior(posicion_posicon);
printf("probando eliminar_posicion\n\n");
eliminar_posicion(posicion_1);
eliminar_posicion(posicion_2);
eliminar_posicion(posicion_posicon);

printf("\n\n\n");
printf("probando lista\n\n");
printf("creando una lista vacia\n");
struct lista * lista_prueba= crear_lista();
printf("Probando agregando elemento a la lista\n");
agregar_elemento(lista_prueba,10);
printf("Probando imprimiendo la lista\n");
imprimir_lista(lista_prueba);
printf("agregando mas elementos elemento a la lista\n");
agregar_elemento(lista_prueba,11);
printf("esta es otra impresión\n\n");
agregar_elemento(lista_prueba,14);
imprimir_lista(lista_prueba);
printf("probando eliminar elemento lista\n\n");
eliminar_elemento_lista(lista_prueba,2);
imprimir_lista(lista_prueba);
printf("esta es otra impresión\n\n");
printf("probando eliminar elemento lista\n\n");
eliminar_elemento_lista(lista_prueba,2);
imprimir_lista(lista_prueba);

agregar_elemento(lista_prueba,14);
agregar_elemento(lista_prueba,11);
printf("Probando buscar dato y su direccion\n\n");
imprimir_lista(lista_prueba);
buscar_dato(lista_prueba,14);
printf("probando buscar k\n");
struct posicion* posicion_prueba=lista_prueba->primero->siguiente;
buscar_k(lista_prueba,posicion_prueba);

printf("probando vaciar\n\n");
vaciar_lista(lista_prueba);
printf("Probando eliminando  lista\n\n");
eliminar_lista(lista_prueba);

return 0;
}